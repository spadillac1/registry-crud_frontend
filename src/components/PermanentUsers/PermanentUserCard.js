import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Button, Card, Icon } from 'semantic-ui-react';

import {
  deletePermanentUser,
  getPermanentUsers,
} from '../../helpers/registry-crud-API';
import PermanentUserModal from './PermanentUserModal';

const ResultsGrid = ({ user, className, onAction }) => {
  const [permanentUser, setPermanentUser] = useState(user);

  useEffect(() => {
    if (user !== undefined) {
      setPermanentUser(user);
    }
  }, [user]);

  const handleDelete = (id) => {
    deletePermanentUser(id)
      .then(() => {
        alert('User was removed');
        getPermanentUsers()
          .then((data) => onAction(data))
          .catch(console.error);
      })
      .catch(console.error);
  };

  const updateUserTrigger = (
    <Button color="blue" icon>
      <Icon name="edit" />
    </Button>
  );

  return (
    <Card>
      <Card.Content>
        <Card.Header>{`${permanentUser.firstName} ${permanentUser.surname}`}</Card.Header>
        <Card.Meta>
          <span className="date">{`${permanentUser.dpi} - ${permanentUser.nominalPosition}`}</span>
        </Card.Meta>
        <Card.Description>
          {`${permanentUser.personalEmail} ${permanentUser.phone}`}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Button
          color="red"
          icon
          onClick={() => handleDelete(permanentUser._id)}
        >
          <Icon name="trash" />
        </Button>
        <PermanentUserModal data={permanentUser} trigger={updateUserTrigger} onClick={onAction} />
      </Card.Content>
    </Card>
  );
};

ResultsGrid.propTypes = {
  results: PropTypes.array.isRequired,
  className: PropTypes.string,
};

export default ResultsGrid;
