import React, { useState, useEffect } from 'react';

import { Card } from 'semantic-ui-react';

import PermanentUserCard from './PermanentUserCard';

const PermanentUserGrid = ({ results, className, onAction }) => {
  const [receivedResults, setReceivedResults] = useState(results);

  useEffect(() => {
    if (results !== undefined) {
      setReceivedResults(results);
    }
  }, [results]);

  return (
    <Card.Group className={className} itemsPerRow={4} doubling>
      {receivedResults.map((result) => (
        <PermanentUserCard key={result.dpi} user={result} onAction={onAction} />
      ))}
    </Card.Group>
  );
};

export default PermanentUserGrid;
