import React, { useState } from 'react';
import { Button, Form, Header, Modal, Icon } from 'semantic-ui-react';

import { useForm } from '../../hooks/useForm';
import { permanentUserForm } from '../../helpers/models';
import {
  getPermanentUsers,
  postPermanentUser,
  putPermanentUser,
} from '../../helpers/registry-crud-API';

import './PermanentUsers.css';

const PermanentUserModal = ({ data, trigger, onClick }) => {
  const [formState, handleInputChange] = useForm(data || permanentUserForm);
  const [open, setOpen] = useState(false);

  const {
    birthdate,
    homePhone,
    institutionalEmail,
    personalEmail,
    phone,
    firstName,
    secondName,
    surname,
    secondSurname,
    marriedSurname,
    dpi,
    nit,
    functionalPosition,
    nominalPosition,
    unitWork,
  } = formState;

  const handleSubmit = () => {
    if (!data) {
      postPermanentUser(formState)
        .then(() => {
          getPermanentUsers(onClick)
            .then((users) => {
              alert('User was created');
              onClick(users);
              setOpen(false);
            })
            .catch(console.error);
        })
        .catch(console.error);
    } else {
      putPermanentUser(formState._id, formState)
        .then(() => {
          getPermanentUsers(onClick)
            .then((users) => {
              alert('User was updated');
              onClick(users);
              setOpen(false);
            })
            .catch(console.error);
          setOpen(false);
        })
        .catch(console.error);
    }
  };

  return (
    <Modal
      basic
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      size="small"
      trigger={trigger}
    >
      <Header icon>
        <Icon name="user" />
        Permanent User Register
      </Header>
      <Modal.Content>
        <Form onSubmit={handleSubmit}>
          <Form.Group widths="equal">
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="text"
              label="First name"
              name="firstName"
              placeholder="First name"
              required
              value={firstName}
            />
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="text"
              label="Second name"
              name="secondName"
              placeholder="Second name"
              value={secondName}
            />
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="text"
              label="Surname"
              name="surname"
              placeholder="Surname"
              required
              value={surname}
            />
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="text"
              label="Second surname"
              name="secondSurname"
              placeholder="Second surname"
              value={secondSurname}
            />
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="text"
              label="Married surname"
              name="marriedSurname"
              placeholder="Married surname"
              value={marriedSurname}
            />
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="date"
              label="Birthdate"
              name="birthdate"
              placeholder="Birthdate"
              required
              value={birthdate}
            />
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="number"
              label="Home pone"
              name="homePhone"
              placeholder="Home phone"
              value={homePhone}
            />
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="number"
              label="Peronsal Phone"
              name="phone"
              placeholder="Personal phone"
              required
              value={phone}
            />
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="email"
              label="Personal Email"
              name="personalEmail"
              placeholder="Personal email"
              required
              value={personalEmail}
            />
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="email"
              label="Institutional Email"
              name="institutionalEmail"
              placeholder="Institutional email"
              required
              value={institutionalEmail}
            />
          </Form.Group>
          <Form.Group widths={3}>
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="number"
              label="DPI"
              name="dpi"
              placeholder="DPI"
              required
              value={dpi}
            />
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="text"
              label="NIT"
              name="nit"
              placeholder="NIT"
              required
              value={nit}
            />
          </Form.Group>
          <Form.Group widths={3}>
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="text"
              label="Functional position"
              name="functionalPosition"
              placeholder="Functional position"
              required
              value={functionalPosition}
            />
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="text"
              label="Nominal position"
              name="nominalPosition"
              placeholder="Nominal position"
              required
              value={nominalPosition}
            />
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="text"
              label="Unit work"
              name="unitWork"
              placeholder="Unit work"
              required
              value={unitWork}
            />
          </Form.Group>
        </Form>
      </Modal.Content>

      <Modal.Actions>
        <Button color="red" inverted onClick={() => setOpen(false)}>
          <Icon name="remove" /> Cancel
        </Button>
        <Button color="green" inverted onClick={handleSubmit}>
          <Icon name="checkmark" /> {data ? 'Update' : 'Registry'}
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

export default PermanentUserModal;
