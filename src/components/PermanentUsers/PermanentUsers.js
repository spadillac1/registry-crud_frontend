import React, { useEffect, useState } from 'react';
import { Button } from 'semantic-ui-react';

import PermanentUserModal from './PermanentUserModal';
import PermanentUserGrid from './PermanentUserGrid';
import { getPermanentUsers } from '../../helpers/registry-crud-API';

import './PermanentUsers.css';

const PermanentUsers = () => {
  const [permanentUsers, setPermanentUsers] = useState([]);

  useEffect(() => {
    getPermanentUsers()
      .then((data) => setPermanentUsers(data))
      .catch((error) => console.error(error.message));
  }, []);

  const addUserTrigger = (
    <Button inverted fluid>
      Add permament user
    </Button>
  );

  return (
    <>
      <PermanentUserModal trigger={addUserTrigger} onClick={setPermanentUsers}/>
      <PermanentUserGrid
        className="results"
        results={permanentUsers}
        onAction={setPermanentUsers}
      />
    </>
  );
};

export default PermanentUsers;
