import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { Button, Card, Icon } from 'semantic-ui-react';

import {
  deleteTemporaryUser,
  getTemporaryUsers,
} from '../../helpers/registry-crud-API';
import TemporaryUserModal from './TemporaryUserModal';

const ResultsGrid = ({ user, className, onAction }) => {
  const [temporaryUser, setTemporaryUser] = useState(user);

  useEffect(() => {
    if (user !== undefined) {
      setTemporaryUser(user);
    }
  }, [user]);

  const handleDelete = (id) => {
    deleteTemporaryUser(id)
      .then(() => {
        alert('User was removed');
        getTemporaryUsers()
          .then((data) => onAction(data))
          .catch(console.error);
      })
      .catch(console.error);
  };

  const updateUserTrigger = (
    <Button color="blue" icon>
      <Icon name="edit" />
    </Button>
  );

  return (
    <Card>
      <Card.Content>
        <Card.Header>{temporaryUser.name}}</Card.Header>
        <Card.Meta>
          <span className="date">{`${temporaryUser.phone} - ${temporaryUser.position}`}</span>
        </Card.Meta>
        <Card.Description>
          {`${temporaryUser.admissionDate} ${temporaryUser.position}`}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Button
          color="red"
          icon
          onClick={() => handleDelete(temporaryUser._id)}
        >
          <Icon name="trash" />
        </Button>
        <TemporaryUserModal data={temporaryUser} trigger={updateUserTrigger} onClick={onAction} />
      </Card.Content>
    </Card>
  );
};

ResultsGrid.propTypes = {
  results: PropTypes.array.isRequired,
  className: PropTypes.string,
};

export default ResultsGrid;
