import React, { useState, useEffect } from 'react';

import { Card } from 'semantic-ui-react';

import TemporaryUserCard from './TemporaryUserCard';

const TemporaryUserGrid = ({ results, className, onAction }) => {
  const [receivedResults, setReceivedResults] = useState(results);

  useEffect(() => {
    if (results !== undefined) {
      setReceivedResults(results);
    }
  }, [results]);

  return (
    <Card.Group className={className} itemsPerRow={4} doubling>
      {receivedResults.map((result) => (
        <TemporaryUserCard key={result.dpi} user={result} onAction={onAction} />
      ))}
    </Card.Group>
  );
};

export default TemporaryUserGrid;
