import React, { useState } from 'react';
import { Button, Form, Header, Modal, Icon } from 'semantic-ui-react';

import { useForm } from '../../hooks/useForm';
import { temporaryUserForm } from '../../helpers/models';
import {
  getTemporaryUsers,
  postTemporaryUser,
  putTemporaryUser,
} from '../../helpers/registry-crud-API';

import './TemporaryUsers.css';

const TemporaryUserModal = ({ data, trigger, onClick }) => {
  const [formState, handleInputChange] = useForm(data || temporaryUserForm);
  const [open, setOpen] = useState(false);

  const { admissionDate, name, phone, place, position } = formState;

  const handleSubmit = () => {
    if (!data) {
      postTemporaryUser(formState)
        .then(() => {
          getTemporaryUsers(onClick)
            .then((users) => {
              alert('User was created');
              onClick(users);
              setOpen(false);
            })
            .catch(console.error);
        })
        .catch(console.error);
    } else {
      putTemporaryUser(formState._id, formState)
        .then(() => {
          getTemporaryUsers(onClick)
            .then((users) => {
              alert('User was updated');
              onClick(users);
              setOpen(false);
            })
            .catch(console.error);
          setOpen(false);
        })
        .catch(console.error);
    }
  };

  return (
    <Modal
      basic
      onClose={() => setOpen(false)}
      onOpen={() => setOpen(true)}
      open={open}
      size="small"
      trigger={trigger}
    >
      <Header icon>
        <Icon name="user" />
        Permanent User Register
      </Header>
      <Modal.Content>
        <Form onSubmit={handleSubmit}>
          <Form.Group widths="equal">
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="text"
              label="Name"
              name="name"
              placeholder="Name"
              required
              value={name}
            />
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="number"
              label="Phone"
              name="phone"
              placeholder="phone"
              required
              value={phone}
            />
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="date"
              label="Admission date"
              name="admissionDate"
              placeholder="Admission date"
              required
              value={admissionDate}
            />
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="text"
              label="Place"
              name="place"
              placeholder="place"
              required
              value={place}
            />
            <Form.Input
              onChange={handleInputChange}
              fluid
              type="text"
              label="Position"
              name="position"
              placeholder="position"
              required
              value={position}
            />

          </Form.Group>
        </Form>
      </Modal.Content>

      <Modal.Actions>
        <Button color="red" inverted onClick={() => setOpen(false)}>
          <Icon name="remove" /> Cancel
        </Button>
        <Button color="green" inverted onClick={handleSubmit}>
          <Icon name="checkmark" /> {data ? 'Update' : 'Registry'}
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

export default TemporaryUserModal;
