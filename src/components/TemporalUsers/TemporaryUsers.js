import React, { useEffect, useState } from 'react';
import { Button } from 'semantic-ui-react';

import TemporaryUserModal from './TemporaryUserModal';
import TemporaryUserGrid from './TemporaryUserGrid';
import { getTemporaryUsers } from '../../helpers/registry-crud-API';

import './TemporaryUsers.css';

const TemporaryUsers = () => {
  const [temporaryUsers, setTemporaryUsers] = useState([]);

  useEffect(() => {
    getTemporaryUsers()
      .then((data) => setTemporaryUsers(data))
      .catch((error) => console.error(error.message));
  }, []);

  const addUserTrigger = (
    <Button inverted fluid>
      Add permament user
    </Button>
  );

  return (
    <>
      <TemporaryUserModal trigger={addUserTrigger} onClick={setTemporaryUsers}/>
      <TemporaryUserGrid
        className="results"
        results={temporaryUsers}
        onAction={setTemporaryUsers}
      />
    </>
  );
};

export default TemporaryUsers;
