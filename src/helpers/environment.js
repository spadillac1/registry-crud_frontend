const URI = 'https://registry-crud-backend.herokuapp.com/api/user';

export const users = {
  permanent: {
    normal: `${URI}/permanent`,
    byId: (id) => `${URI}/permanent/${id}`,
  },
  temporary: {
    normal: `${URI}/temporary`,
    byId: (id) => `${URI}/temporary/${id}`,
  },
};
