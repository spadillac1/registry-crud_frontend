import firebase from 'firebase';

export const firebaseConfig = {
  apiKey: 'AIzaSyBvJFyOQlTV_X08gEasxlSg0i1E4pvtAVg',
  authDomain: 'facesec-b6c07.firebaseapp.com',
  databaseURL: 'https://facesec-b6c07.firebaseio.com',
  projectId: 'facesec-b6c07',
  storageBucket: 'facesec-b6c07.appspot.com',
  messagingSenderId: '1004848011421',
  appId: '1:1004848011421:web:0d676828b74748114d9c74',
  measurementId: 'G-VMJSHX8CJJ',
};

export const initializeFirebase = () => {
  firebase.initializeApp(firebaseConfig);
};

export const signInWithGoogle = () => {
  let provider = 
    new firebase
    .auth
    .GoogleAuthProvider()
    .addScope('https://www.googleapis.com/auth/contacts.readonly');
  return firebase.auth().signInWithPopup(provider);
}

export const signOut = () => {
  return firebase.auth().signOut();
}
