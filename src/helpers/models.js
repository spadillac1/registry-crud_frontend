export const permanentUserForm = {
  birthdate: '',
  homePhone: '',
  institutionalEmail: '',
  personalEmail: '',
  phone: '',
  firstName: '',
  secondName: '',
  surname: '',
  secondSurname: '',
  marriedLastName: '',
  dpi: '',
  nit: '',
  funcionalPosition: '',
  nominalPosition: '',
  unitWork: '',
};

export const temporaryUserForm = {
  admissionDate: '',
  name: '',
  phone: '',
  place: '',
  position: '',
};
