import axios from 'axios';

import { users } from './environment';

const getUsers = (route) => {
  return axios.get(route).then((res) => {
    if (res.status === 200) {
      return res.data;
    } else {
      throw new Error('Incorrect Code Number');
    }
  });
};

const deleteUser = (route) => {
  return axios.delete(route).then((res) => {
    if (res.status === 200) {
      return res.data;
    } else {
      throw new Error('Incorrect Code Number');
    }
  });
};

const putUser = (route, data) => {
  return axios.put(route, data).then((res) => {
    if (res.status === 200) {
      return res.data;
    } else {
      throw new Error('Incorrect Code Number');
    }
  });
};

const postUser = (route, data) => {
  return axios.post(route, data).then((res) => {
    if (res.status === 200) {
      return res.data;
    } else {
      throw new Error('Incorrect Code Number');
    }
  });
};

export const getPermanentUsers = () => getUsers(users.permanent.normal);
export const deletePermanentUser = (id) => deleteUser(users.permanent.byId(id));
export const putPermanentUser = (id, user) => putUser(users.permanent.byId(id), user);
export const postPermanentUser = (user) => postUser(users.permanent.normal, user);

export const getTemporaryUsers = () => getUsers(users.temporary.normal);
export const deleteTemporaryUser = (id) => deleteUser(users.temporary.byId(id));
export const putTemporaryUser = (id, user) => putUser(users.temporary.byId(id), user);
export const postTemporaryUser = (user) => postUser(users.temporary.normal, user);
