import app from 'firebase/app';
import 'firebase/auth';

const config = {
  apiKey: 'AIzaSyDgx-cNTU0AHRABS906JBDRcGE0JZRXbaw',
  authDomain: 'marvels-universe.firebaseapp.com',
  databaseURL: 'https://marvels-universe.firebaseio.com',
  projectId: 'marvels-universe',
  storageBucket: 'marvels-universe.appspot.com',
  messagingSenderId: '1080494408725',
  appId: '1:1080494408725:web:54ecbfd866d7afd6b43a1c',
};

export const firebase = app.initializeApp(config);
