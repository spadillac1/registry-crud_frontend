import React, { Suspense, lazy } from 'react';
import ReactDOM from 'react-dom';
import { initializeFirebase } from './helpers/firebase';
import { Loader } from 'semantic-ui-react';

import 'semantic-ui-css/semantic.min.css';
import './index.css';

const AppRouter = lazy(() => import('./routers/AppRouter'));

initializeFirebase();

ReactDOM.render(
  <Suspense
    fallback={
      <Loader inverted active>
        Loading...
      </Loader>
    }
  >
    <AppRouter />
  </Suspense>,
  document.getElementById('root')
);
