import React, { lazy } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';

const LoginRouter = () => {
  return (
    <Router>
      <Switch>

        <Route
          name="app"
          path="/app"
          component={lazy(() => import('../views/App'))}
        />

        <Route
          exact
          name="login"
          path="/login"
          component={lazy(() => import('../views/Login'))}
        />

        <Redirect from="/" to="/login" />
      </Switch>
    </Router>
  );
};

// <Redirect from="/" to="login" />
export default LoginRouter;
