import React from 'react';
import { shallow } from 'enzyme';

import  App  from '../views/App';

describe('tests for <App />', function () {
  const wrapper = shallow(<App />);

  it('should match with snapshot', () => {
    expect(wrapper).toMatchSnapshot();
  });
});
