import React from 'react';
import { Grid, Header, Tab, Button } from 'semantic-ui-react';
import TemporaryUsers from '../components/TemporalUsers/TemporaryUsers';
import PermanentUsers from '../components/PermanentUsers/PermanentUsers';
import { signOut } from '../helpers/firebase';

import './App.css';

const App = ({ history }) => {
  const panes = [
    {
      menuItem: 'Permanent Users',
      render: () => (
        <Tab.Pane inverted attached="top">
          <PermanentUsers />
        </Tab.Pane>
      ),
    },
    {
      menuItem: 'Temporary Users',
      render: () => (
        <Tab.Pane inverted attached="top">
          <TemporaryUsers />
        </Tab.Pane>
      ),
    },
  ];
  const signOutGoogle = () => {
    signOut().then(() => history.replace('login'));
  };

  return (
    <>
      <Button color="blue" className="app__logout" onClick={signOutGoogle}>
        Sing Out
      </Button>

      <Grid className="app">
        <Grid.Column className="app__column">
          <Header inverted as="h1">
            User Registry
          </Header>
          <Tab
            menu={{ secondary: true, inverted: true, pointing: true }}
            panes={panes}
          />
        </Grid.Column>
      </Grid>
    </>
  );
};

export default App;
