import React from 'react';
import { Button, Container, Form, Grid, Header} from 'semantic-ui-react';

import { useForm } from '../hooks/useForm';
import { signInWithGoogle } from '../helpers/firebase';

import './Login.css';

const Login = ({ history }) => {
  const [formSatate, handleInputChange] = useForm({
    email: '',
    password: '',
  });
  const { email, password } = formSatate;

  /*   const handleSignIn = (enteredEmail, enteredPassword) => { */
  // if (enteredEmail !== '' && enteredPassword !== '') {
  // (enteredEmail, enteredPassword)
  // .then(() => {
  // history.replace('app');
  // })
  // .catch((error) => alert(error.message));
  // }
  /* }; */
  const handleSignIn = () => {
    signInWithGoogle()
      .then(() => {
        history.replace('app');
      })
      .catch((error) => alert(error.message));
  };

  return (
    <Container>
      <Grid className="login" centered verticalAlign="middle">
        <Grid.Column className="login__column">
          <Header className="login__title" as='h1' inverted>Control de Usuarios</Header>
          <Form className="login__form">
            <label htmlFor="email">Email</label>
            <Form.Input
              onChange={handleInputChange}
              autoComplete="off"
              id="email"
              name="email"
              placeholder="email@example.com"
              type="email"
              value={email}
            />
            <label htmlFor="password">Password</label>
            <Form.Input
              onChange={handleInputChange}
              autoComplete="off"
              id="password"
              name="password"
              placeholder="&bull;&bull;&bull;&bull;&bull;"
              type="password"
              value={password}
            />
            <Button
              onClick={() => handleSignIn(email, password)}
              className="login__button-login"
              color="red"
              fluid
              type="submit"
              inverted
            >
              Sign In With Google
            </Button>
          </Form>
        </Grid.Column>
      </Grid>
    </Container>
  );
};

export default Login;
